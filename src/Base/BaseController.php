<?php

namespace Base;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;

class BaseController extends Controller{

    protected $response;

    protected function generateResponse($code){
        $this->response = new Response();
        $this->response->setStatusCode($code);
        $this->response->headers->set('Content-type', 'application/json');
    }


    protected function setContent($content){
        $this->response->setContent(json_encode($content));
    }

    protected function getResponse(){
        return $this->response;
    }
}