<?php

namespace Client\ProductsBundle\Controller;

use Client\ProductsBundle\Form\ProductsType;
use Symfony\Component\Form\Form;
use Symfony\Component\Serializer\Serializer;
use Server\ProductsBundle\Entity\Products;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Config\Definition\Exception\Exception;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Normalizer\GetSetMethodNormalizer;

class DefaultController extends Controller
{
    const INVALID_DATA = 'Invalid form data';
    const API_ERROR = 'API error';
    const REMOVE_ERROR = 'Removing item error';
    const RECORD_NOT_FOUND = 'Record not found';

    /**
     * Products list
     * @return Response
     * @throws \Symfony\Component\Config\Definition\Exception\Exception
     */
    public function indexAction(){
        $response = $this->get('api')->get();
        if (Response::HTTP_OK != $response->getStatusCode()){
            throw new Exception('Error getting products', $response->getStatusCode());
        }
        $item_list = json_decode($response->getBody(true), true);
        return $this->render('ClientProductsBundle:Default:index.html.twig', array('item_list' => $item_list) );
    }


    /**
     * Shows create form
     * @return Response
     */
    public function createAction(){
        $form = $this->createForm(new ProductsType($this->generateUrl('client_products_create_submit')), new Products());

        return $this->render('ClientProductsBundle:Default:form.html.twig', array('form' => $form->createView()));
    }


    /**
     * Create form handler
     * @param Request $request
     * @return RedirectResponse|Response
     */
    public function createSubmitAction(Request $request){
        $form = $this->createForm(new ProductsType(), new Products());
        $form->handleRequest($request);
        if (!$form->isValid()){
            return $this->showError(self::INVALID_DATA);
        }

        $response = $this->sendCreateRequest($form);
        if (Response::HTTP_CREATED != $response->getStatusCode()){
            return $this->showError(self::API_ERROR);
        }

        return new RedirectResponse($this->generateUrl('client_products_list'));
    }


    /**
     * Sends create request
     * @param Form $form
     * @return \Guzzle\Http\Message\Response
     */
    private function sendCreateRequest(Form $form){
        $serialized_entity = $this->serializeEntity($form->getData());
        return $this->get('api')->post($serialized_entity);
    }


    /**
     * Shows error
     * @param null $message
     * @return Response
     */
    public function showError($message = null){
        return $this->render('ClientProductsBundle:Default:error.html.twig', array('message' => $message));
    }


    /**
     * Serializes entity to json
     * @param Products $entity
     * @return string|\Symfony\Component\Serializer\Encoder\scalar
     */
    private function serializeEntity(Products $entity){
        $serializer = new Serializer(array(new GetSetMethodNormalizer()), array('json' => new JsonEncoder()) );
        return $serializer->serialize($entity, 'json');
    }


    /**
     * Deserializes json to entity
     * @param $json
     * @return object
     */
    private function deserializeEntity($json){
        $serializer = new Serializer(array(new GetSetMethodNormalizer()), array('json' => new JsonEncoder()));
        return $serializer->denormalize(json_decode($json, true), 'Server\ProductsBundle\Entity\Products');
    }


    /**
     * Removes product
     * @param $id
     * @return RedirectResponse|Response
     */
    public function removeAction($id){
        $response = $this->get('api')->delete($id);
        if (Response::HTTP_NO_CONTENT != $response->getStatusCode()){
            return $this->showError(self::REMOVE_ERROR);
        }

        return new RedirectResponse($this->generateUrl('client_products_list'));
    }


    /**
     * Shows product update form
     * @param $id
     * @return object|Response
     */
    public function updateAction($id){
        $product = $this->getRecord($id);
        if (!($product instanceof Products)){
            return $product;
        }
        $form = $this->createForm(new ProductsType(), $product);

        return $this->render('ClientProductsBundle:Default:form.html.twig', array('form' => $form->createView()));
    }


    /**
     * Update form handler
     * @param $id
     * @param Request $request
     * @return RedirectResponse|Response
     */
    public function updateSubmitAction($id, Request $request){
        $form = $this->createForm(new ProductsType(), new Products());
        $form->handleRequest($request);

        if (!$form->isValid()){
            return $this->showError(self::INVALID_DATA);
        }

        $response = $this->get('api')->put($id, $this->serializeEntity($form->getData()));
        if (Response::HTTP_OK != $response->getStatusCode()){
            return $this->showError(self::INVALID_DATA);
        }

        return new RedirectResponse($this->generateUrl('client_products_list'));
    }


    /**
     * Return product entity
     * @param $id
     * @return object|Response
     */
    private function getRecord($id){
        $response = $this->get('api')->get($id);
        if (Response::HTTP_NOT_FOUND == $response->getStatusCode()){
            return $this->showError(self::RECORD_NOT_FOUND);
        }

        return $this->deserializeEntity($response->getBody(true));
    }


    /**
     * Shows product
     * @param $id
     * @return object|Response
     */
    public function showAction($id){
        $product = $this->getRecord($id);
        if (!($product instanceof Products)){
            return $product;
        }

        return $this->render('ClientProductsBundle:Default:show.html.twig', array(
            'record' => $product
        ));
    }
}
