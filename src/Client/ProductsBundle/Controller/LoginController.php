<?php

namespace Client\ProductsBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class LoginController extends Controller{

    public function indexAction(){
        return $this->render('ClientProductsBundle:Login:form.html.twig');
    }
}