<?php

namespace Client\ProductsBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Validator\Constraints\NotBlank;

class ProductsType extends AbstractType{

    private $action;

    public function __construct($action = null){
        $this->action = $action;
    }

    public function buildForm(FormBuilderInterface $builder, array $options){
        $builder->setAction($this->action);
        $builder->add('title', 'text', array(
            'constraints' => array(
                new NotBlank()
            )
        ));
        $builder->add('shortDesc', 'textarea');
        $builder->add('price', 'money');
    }

    public function getName(){
        return 'products';
    }

    public function getAction(){
        return 'someaction';
    }
}