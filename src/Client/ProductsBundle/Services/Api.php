<?php

namespace Client\ProductsBundle\Services;

use Guzzle\Http\Client;

class Api{

    protected $base_url = 'http://learn.local';
    protected $sub_url = '/products';
    protected $client;
    protected $request;


    public function __construct(){
        $this->client = new Client($this->base_url);
    }


    /**
     * Setting base url
     * @param $url
     */
    public function setBaseUrl($url){
        $this->base_url = $url;
    }


    /**
     * Setting sub url
     * @param $url
     */
    public function setSubUrl($url){
        $this->sub_url = $url;
    }


    /**
     * Sending get request
     * @param null $id
     * @return \Guzzle\Http\Message\Response
     */
    public function get($id = null){
        if ($id != null){
            $this->request = $this->client->get($this->sub_url . '/' . $id);
        }
        else{
            $this->request = $this->client->get($this->sub_url);
        }

        return $this->request->send();
    }


    /**
     * Sending post request
     * @param $data
     * @return \Guzzle\Http\Message\Response
     */
    public function post($data){
        $this->request = $this->client->post($this->sub_url, array(
            'Content-type' => 'application/json'
        ), $data);
        return $this->request->send();
    }


    /**
     * Sending put request
     * @param $id
     * @param $data
     * @return \Guzzle\Http\Message\Response
     */
    public function put($id, $data){
        $this->request = $this->client->put($this->sub_url . '/' . $id, array(
            'Content-type' => 'application/json'
        ), $data);
        return $this->request->send();
    }


    /**
     * Sending delete request
     * @param $id
     * @return \Guzzle\Http\Message\Response
     */
    public function delete($id){
        $this->request = $this->client->delete($this->sub_url . '/' . $id, array(
            'Content-type' => 'application/json'
        ));
        return $this->request->send();
    }


    /**
     * Return last request
     * @return mixed
     */
    public function getRequest(){
        return $this->request;
    }
}