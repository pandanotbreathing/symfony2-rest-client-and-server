<?php

namespace Server\ProductsBundle\Controller;

use Base\BaseController;
//use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Config\Definition\Exception\Exception;
use Symfony\Component\Serializer\Serializer;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Normalizer\GetSetMethodNormalizer;
use Server\ProductsBundle\Entity\Products;

class DefaultController extends BaseController
{
    protected $repository_name = 'ServerProductsBundle:Products';

    public function indexAction()
    {
        return $this->render('ServerProductsBundle:Default:index.html.twig', array('name' => 'we'));
    }


    /**
     * Creates product
     * @param Request $request
     * @return mixed
     */
    public function createAction(Request $request){
        try{
            $product = $this->serializeProduct($request);
        }
        catch(Exception $e){
            $this->generateResponse(Response::HTTP_BAD_REQUEST);
            return $this->getResponse();
        }

        $manager = $this->getDoctrine()
                        ->getManager();
        $manager->persist($product);
        $manager->flush();
        $this->generateResponse(Response::HTTP_CREATED);
        return $this->getResponse();
    }


    /**
     * Serializes request to products entity
     * @param Request $request
     * @return object product entity
     */
    protected function serializeProduct(Request $request){
        $serializer = new Serializer(array(new GetSetMethodNormalizer()), array('json' => new JsonEncoder()));
        $decoded = $serializer->decode($request->getContent(), 'json');
        if (!array_key_exists('title', $decoded)){
            throw new Exception();
        }

        if (array_key_exists('price', $decoded) && !preg_match('/^\d+$/', $decoded['price'])){
            throw new Exception();
        }

        return $serializer->denormalize($decoded, 'Server\ProductsBundle\Entity\Products');
    }


    /**
     * Updates record with id
     * @param $id product id
     * @param Request $request
     * @return mixed
     */
    public function updateAction($id, Request $request){
        $product = $this->getProduct($id);

        if (!$product){
            $this->generateResponse(Response::HTTP_NOT_FOUND);
            return $this->getResponse();
        }

        $incoming_product = json_decode($request->getContent(), true);
        $product->setOptions($incoming_product);
        $manager = $this->getDoctrine()
                        ->getManager();
        $manager->persist($product);
        $manager->flush();
        $this->generateResponse(Response::HTTP_OK);
        return $this->getResponse();
    }


    /**
     * Returns product object
     * @param $id product id
     * @return object product
     */
    protected function getProduct($id){
        return $this->getDoctrine()
                    ->getRepository($this->repository_name)
                    ->find($id);
    }


    /**
     * Returns product record
     * @param $id product id
     * @return mixed
     */
    public function getAction($id){
        $product = $this->getProduct($id);

        if (!$product){
            $this->generateResponse(Response::HTTP_NOT_FOUND);
            return $this->getResponse();
        }

        $serializer = new Serializer(array(new GetSetMethodNormalizer()), array('json' => new JsonEncoder()));
        $this->generateResponse(Response::HTTP_OK);
        $this->setContent(json_decode($serializer->serialize($product, 'json'), true));
        return $this->getResponse();
    }


    /**
     * Returns products list
     * @return mixed
     */
    public function getListAction(){
        $products_list = $this->getDoctrine()
                       ->getRepository($this->repository_name)
                       ->findAll();
        $serializer = new Serializer(array(new GetSetMethodNormalizer()), array('json' => new JsonEncoder()));
        foreach ($products_list as $key => $value){
            $products_list[$key] = json_decode($serializer->serialize($value, 'json'), true);
        }
        $this->generateResponse(Response::HTTP_OK);
        $this->setContent($products_list);
        return $this->getResponse();
    }


    /**
     * Removes product
     * @param $id product id
     * @return mixed
     */
    public function removeAction($id){
        $products = $this->getProduct($id);

        if (!$products){
            $this->generateResponse(Response::HTTP_NOT_FOUND);
            return $this->getResponse();
        }

        $manager = $this->getDoctrine()
                        ->getManager();
        $manager->remove($products);
        $manager->flush();

        $this->generateResponse(Response::HTTP_NO_CONTENT);
        return $this->getResponse();
    }
}
