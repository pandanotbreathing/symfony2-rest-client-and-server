<?php

namespace Server\ProductsBundle\Tests\Controller;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\HttpFoundation\Response;

class DefaultControllerTest extends WebTestCase
{
//    public function testIndex()
//    {
//        $client = static::createClient();
//
//        $crawler = $client->request('GET', '/hello/Fabien');
//
//        $this->assertTrue($crawler->filter('html:contains("Hello Fabien")')->count() > 0);
//    }

    public function testGetList(){
        $client = static::createClient();
        $client->request('GET', '/products');

        $code = $client->getResponse()->getStatusCode();
        $response = $client->getResponse()->getContent();

        $this->assertEquals(Response::HTTP_OK, $code);
        $this->assertTrue(is_array(json_decode($response, true)));
    }


    public function testCreateEmptyRequest(){
        $client = static::createClient();
        $client->request(
            'POST',
            '/products',
            array(),
            array(),
            array('CONTENT-TYPE' => 'application/json'),
            json_encode(array())
        );

        $this->assertEquals(Response::HTTP_BAD_REQUEST, $client->getResponse()->getStatusCode());
    }

    public function testCreateEmptyTitle(){
        $client = static::createClient();
        $send_array = array(
            'shortDesc' => 'newSde'
        );
        $client->request(
            'POST',
            '/products',
            array(),
            array(),
            array('CONTENT-TYPE' => 'application/json'),
            json_encode($send_array)
        );

        $this->assertEquals(Response::HTTP_BAD_REQUEST, $client->getResponse()->getStatusCode());
    }

    public function testCreateSuccessRequest(){
        $client = static::createClient();
        $send_array = array(
            'title' => 'sometitle'
        );
        $client->request(
            'POST',
            '/products',
            array(),
            array(),
            array('CONTENT-TYPE' => 'application/json'),
            json_encode($send_array)
        );

        $this->assertEquals(Response::HTTP_CREATED, $client->getResponse()->getStatusCode());
    }


    public function testCreateSuccessRequestAllFields(){
        $client = static::createClient();
        $send_array = array(
            'title' => 'someonenew',
            'shortDesc' => 'someDesc',
            'price' => 400
        );
        $client->request(
            'POST',
            '/products',
            array(),
            array(),
            array('CONTENT-TYPE' => 'application/json'),
            json_encode($send_array)
        );

        $this->assertEquals(Response::HTTP_CREATED, $client->getResponse()->getStatusCode());
    }

    public function testCreatePriceString(){
        $client = static::createClient();
        $send_array = array(
            'title' => 'someonenew',
            'shortDesc' => 'someDesc',
            'price' => 'wrongvalue'
        );
        $client->request(
            'POST',
            '/products',
            array(),
            array(),
            array('CONTENT-TYPE' => 'application/json'),
            json_encode($send_array)
        );

        $this->assertEquals(Response::HTTP_BAD_REQUEST, $client->getResponse()->getStatusCode());
    }


    public function testUpdateEmptyRequest(){
        $client = static::createClient();
        $client->request(
            'PUT',
            '/products/1',
            array(),
            array(),
            array('CONTENT-TYPE' => 'application/json'),
            json_encode(array())
        );

        $this->assertEquals(Response::HTTP_OK, $client->getResponse()->getStatusCode());
    }


    public function testUpdateNotFoundRecord(){
        $client = static::createClient();
        $client->request(
            'PUT',
            '/products/9999',
            array(),
            array(),
            array('CONTENT-TYPE' => 'application/json'),
            json_encode(array())
        );

        $this->assertEquals(Response::HTTP_NOT_FOUND, $client->getResponse()->getStatusCode());
    }


    public function testGetFound(){
        $client = static::createClient();
        $client->request(
            'GET',
            '/products/5'
        );

        $this->assertEquals(Response::HTTP_OK, $client->getResponse()->getStatusCode());
        $this->assertEquals('application/json', $client->getResponse()->headers->get('Content-type'));
        $this->assertEquals(true, count(json_decode($client->getResponse()->getContent())) > 0);
    }

    public function testGetNotFound(){
        $client = static::createClient();
        $client->request(
            'GET',
            '/products/9999'
        );

        $this->assertEquals(Response::HTTP_NOT_FOUND, $client->getResponse()->getStatusCode());
    }


    public function testRemoveNotFound(){
        $client = static::createClient();
        $client->request(
            'DELETE',
            '/products/9999'
        );

        $this->assertEquals(Response::HTTP_NOT_FOUND, $client->getResponse()->getStatusCode());
    }

    public function testRemoveAction(){
        $client = static::createClient();
        $client->request(
            'DELETE',
            '/products/7'
        );

        $this->assertEquals(Response::HTTP_NO_CONTENT, $client->getResponse()->getStatusCode());
    }
}
